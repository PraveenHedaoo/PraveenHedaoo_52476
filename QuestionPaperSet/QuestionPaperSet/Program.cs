﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionPaperSet
{
    class Program
    {
        List<string> ASet = new List<string> {}; //Question Paper SetA
        List<string> BSet = new List<string> {}; //Question Paper SetB
        List<string> CSet = new List<string> {}; //Question Paper SetC
        List<string> DSet = new List<string> {}; //Question Paper SetD
        List<string> QSet = new List<string> {}; //Question Paper SetQ for Storing Final Set of Questions

        static void Main(string[] args)
        {                     
            Program obj = new Program();
            
            //Check if set is empty
            bool a = false;
            bool b = false;
            bool c = false;
            bool d = false;

            //check repeated questions are not entered
            bool a1 = false;
            bool b1 = false;
            bool c1 = false;
            bool d1= false;
          

            //Generating Questions
            for(int i = 0; i < 11000; i++)
            {
                string add = "A" + i;
                obj.ASet.Add(add);
            }

            for (int i = 0; i < 10000; i++)
            {                
                string add1 = "B" + i;
                obj.BSet.Add(add1);              
            }
            for (int i = 0; i < 30000; i++)
            {
               
                string add2 = "C" + i;
                obj.CSet.Add(add2);
              
            }
            for (int i = 0; i < 9000; i++)
            {

                string add3 = "D" + i;
                obj.DSet.Add(add3);

            }

            //Total Questions
            int res = obj.ASet.Count + obj.BSet.Count + obj.CSet.Count + obj.DSet.Count;

            //No of Questions in Each Set
            int count1 = obj.ASet.Count;
            int count2 = obj.BSet.Count;
            int count3 = obj.CSet.Count;
            int count4 = obj.DSet.Count;
            
            Stopwatch sw = Stopwatch.StartNew();

            for (int i = 0; i < res; i++)
            {
                
                if (c == false && c1 == false)
                {

                    obj.QSet.Add(obj.CSet[obj.CSet.Count - count3]);
                    count3--;
                    a1 = false;
                    b1 = false;
                    c1 = true;
                    d1 = false;
                    if (count3 == 0)
                    {
                        c = true;
                    }
                    Console.Write(obj.QSet[i]);
                    continue;
                }


                if (d == false && d1 == false)
                {
                    obj.QSet.Add(obj.DSet[obj.DSet.Count - count4]);
                    count4--;
                    a1 = false;
                    b1 = false;
                    c1 = false;
                    d1 = true;
                    if (count4 == 0)
                    {
                        d = true;
                    }
                    Console.Write(obj.QSet[i]);
                    continue;
                }
                if (a == false && a1==false)
                    {
                        obj.QSet.Add(obj.ASet[obj.ASet.Count-count1]);
                        count1--;
                    a1 = true;
                    b1 = false;
                    c1 = false;
                    d1 = false;

                    if (count1== 0)
                    {
                        a = true;
                    }
                    Console.Write(obj.QSet[i]);
                    continue;
                    }
                
               
                    if (b == false && b1 == false)
                    {
                        obj.QSet.Add(obj.BSet[obj.BSet.Count-count2]);
                        count2--;
                    a1 = false;
                    b1 = true;
                    c1 = false;
                    d1 = false;
                    if (count2 == 0)
                    {
                        b = true;
                    }
                    Console.Write(obj.QSet[i]);
                    continue;
                    }


                

            }            
            sw.Stop();
            Console.WriteLine("Time taken: {0}ms", sw.Elapsed.TotalMilliseconds);

            Console.ReadKey();
        }
    }
}
