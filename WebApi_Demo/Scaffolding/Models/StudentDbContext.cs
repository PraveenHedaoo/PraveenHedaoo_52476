﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApi_Demo.Models;

namespace Scaffolding.Models
{
    public class StudentDbContext:DbContext
    {
        public DbSet<StudentViewModel> StudentViewModels { get; set; }

        public System.Data.Entity.DbSet<WebApi_Demo.student> students { get; set; }
    }
}