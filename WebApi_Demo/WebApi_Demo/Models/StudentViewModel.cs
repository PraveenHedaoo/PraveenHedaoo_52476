﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi_Demo.Models
{
    public class StudentViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> join_date { get; set; }
    }
}